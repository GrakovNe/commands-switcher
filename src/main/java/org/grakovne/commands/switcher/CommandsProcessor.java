package org.grakovne.commands.switcher;

import com.google.common.base.Strings;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class CommandsProcessor {
    private final String COMMANDS_FILE_NAME = "commands.conf";
    private final static Logger logger = Logger.getLogger(CommandsProcessor.class);

    public int getCommandsNumber() {
        LineNumberReader reader = null;
        Integer result = 0;
        try {
            reader = new LineNumberReader(new FileReader(getFile()));
            if (Strings.isNullOrEmpty(reader.readLine())) {
                result = 0;
            } else {
                reader.skip(Long.MAX_VALUE);
                result = reader.getLineNumber() + 1;
            }

        } catch (IOException e) {
            logger.error("can't read commands file.");
        }
        return result;
    }

    public String getCommand(Integer commandNumber) {
        LineNumberReader reader;

        String result = null;
        Integer totalCommands = getCommandsNumber() - 1;

        try {
            reader = new LineNumberReader(new FileReader(getFile()));

            for (int i = 0; i < commandNumber % totalCommands; i++) {
                reader.readLine();
            }

            result = reader.readLine();
        } catch (IOException e) {
            logger.error("can't read commands file.");
        }

        return result;
    }

    private File getFile() {
        File result = new File(COMMANDS_FILE_NAME);

        if (!result.exists()) {
            try {
                result.createNewFile();
                logger.warn("commands file is recreated.");
            } catch (IOException e) {
                logger.error("can't create commands file.");
            }
        }

        return result;
    }
}
