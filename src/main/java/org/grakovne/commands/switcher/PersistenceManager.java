package org.grakovne.commands.switcher;

import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

public class PersistenceManager {

    private final String PERSISTENCE_STATE_FILE_NAME = "persistence.dat";
    private final static Logger logger = Logger.getLogger(PersistenceManager.class);

    public Integer getCurrentCommandNumber() {
        LineNumberReader reader;
        Integer result = 0;

        try {
            reader = new LineNumberReader(new FileReader(getFile()));
            result = Integer.valueOf(reader.readLine());
        } catch (IOException | NumberFormatException | NullPointerException ex) {
            logger.error("can't read persistence file.");
        }

        return result;
    }

    public void incrementCommandNumber() {
        Integer nextCommandNumber = getCurrentCommandNumber() + 1;

        try (BufferedWriter writer = Files.newBufferedWriter(getFile().toPath(), StandardOpenOption.TRUNCATE_EXISTING);) {
            writer.write(String.valueOf(nextCommandNumber));
        } catch (IOException e) {
            logger.error("can't write persistence file.");
        }

    }

    private File getFile() {
        File result = new File(PERSISTENCE_STATE_FILE_NAME);

        if (!result.exists()) {
            try {
                result.createNewFile();
                logger.warn("persistence file is recreated.");
            } catch (IOException e) {
                logger.error("can't create persistence file.");
            }
        }

        return result;
    }


}
