package org.grakovne.commands.switcher;

import org.apache.log4j.Logger;

public class Switcher {

    private final static Logger logger = Logger.getLogger(Switcher.class);

    public static void main(String[] args) {
        PersistenceManager manager = new PersistenceManager();
        CommandsProcessor processor = new CommandsProcessor();

        if (processor.getCommandsNumber() == 0){
            logger.error("commands file is empty.");
            return;
        }

        String currentCommand = processor.getCommand(manager.getCurrentCommandNumber());
        String commandResponse = CommandsExecutor.executeCommand(currentCommand);

        manager.incrementCommandNumber();

        logger.info(currentCommand + " : " + commandResponse);
    }
}
