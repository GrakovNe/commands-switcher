package org.grakovne.commands.switcher;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandsExecutor {
    private final static Logger logger = Logger.getLogger(CommandsProcessor.class);

    public static String executeCommand(String command) {

        StringBuilder result = new StringBuilder();

        try {
            Runtime r = Runtime.getRuntime();
            Process p = r.exec(command);
            p.waitFor();
            BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = b.readLine()) != null) {
                result.append(line);
            }

            b.close();
        } catch (IOException | InterruptedException ex) {
            logger.error("can't execute command completely.");
        }

        return result.toString();
    }
}
